/* eslint-env node */

import path from "path";
import url from "url";

import express from "express";

const PORT = 4444;

let app = express();
let dirname = path.dirname(url.fileURLToPath(import.meta.url));

app.use(express.static(path.join(dirname, "page")));
app.listen(PORT, "localhost", () =>
{
  // eslint-disable-next-line no-console
  console.log(`Test pages server listening at http://localhost:${PORT}`);
});
