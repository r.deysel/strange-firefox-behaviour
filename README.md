# Firefox extension behaviour

This simple project is to showcase some interesting behaviour in Firefox as it relates to: https://bugzilla.mozilla.org/show_bug.cgi?id=1688967

Getting it set up:
- Install: `npm i`
- Start a simple local server: `node start-server.js`. This allows for a simple test to get this working.
- Load the extension in Firefox using debug mode. To do this, go t Add-ons. 
- Click on the cog and select "Debug Addons" then click on "Load Temporary Add-on" and select any file inside of the extension's folder.  
- The extension will automatically open the example page called `firefoxbug.html`
- Inspect the background page's logs.
- The expected behaviour is that the onError event would be emitted before the onCompleted event.
- The timestamp shown in the details object also shows that the error event happens before the completed event.
- Very keen to better understand why this might be or what I might be misunderstanding.  
- The main source of confusion comes from the order that these events are fired in.
- Please note, this extention will block all requests from Firefox. Please remove after use. 
