"use strict";

let examplePageUrl = "http://localhost:4444/firefoxbug.html";
let exampleImageUrl = "http://localhost:4444/image.png";

chrome.tabs.create({url: examplePageUrl});

let shouldLog = ({url}) => {
  return url == examplePageUrl || url == exampleImageUrl
}

function onBeforeRequest(details) {
  if(details.url == exampleImageUrl)
    return {cancel: true}
}

chrome.webRequest.onBeforeRequest.addListener(
  onBeforeRequest,
  {
    types: Object.values(chrome.webRequest.ResourceType)
                 .filter(type => type != "main_frame"),
    urls: ["http://*/*", "https://*/*", "ws://*/*", "wss://*/*"]
  },
  ["blocking"]
);

chrome.webRequest.onErrorOccurred.addListener(
  details => {  
    if (shouldLog(details))
    {
      console.log("-------------1. ");
      console.log("onErrorOccurred", "page time: " + performance.now(), details);
    }
  }
  , {urls: ["<all_urls>"]}
);

chrome.webNavigation.onCompleted.addListener(
  details => {  
    if (shouldLog(details))
    {
      console.log("-------------2.");
      console.log("onCompleted", "page time: " + performance.now(), details);
    }
  }
);

